//
//  RepositoryDAOTests.swift
//  AppiOS
//
//  Created by Judar Lima on 18/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import XCTest

@testable import AppiOS

class RepositoryDAOTests: XCTestCase {
    
    func testFetchRepositoryPullRequests() {
        
        var pullRequests: [PullRequest] = []
        
        let asyncExpectation = expectation(description: "User get request return")
        
        let completionBlock = { (pulls: [PullRequest]) -> Void  in
                    pullRequests = pulls
                    asyncExpectation.fulfill()
                }
        
        RepositoryDAO.fetchRepositoryPullRequests(login: "Alamofire", name: "Alamofire", completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0){ (error) in
            XCTAssert(pullRequests.count > 0)
        }
        
    }
    
    func testFetchRepositoriesByStars() {

        var repositories: [Repository] = []
        
        let asyncExpectation = expectation(description: "User get request return")
        
        let completionBlock = { (repos: [Repository]) -> Void  in
            repositories = repos
            asyncExpectation.fulfill()
        }
        
        RepositoryDAO.fetchRepositoriesByStars(language: "Swift", page: 1, completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0){ (error) in
            XCTAssert(repositories.count > 0)
        }

    }
    
    func testFetchUnexistentRepositoryPullRequests() {
        
        var pullRequests: [PullRequest] = []
        
        let asyncExpectation = expectation(description: "User get request return")
        
        let completionBlock = { (pulls: [PullRequest]) -> Void  in
            pullRequests = pulls
            asyncExpectation.fulfill()
        }
        
        RepositoryDAO.fetchRepositoryPullRequests(login: "blabla", name: "blabla", completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0){ (error) in
            XCTAssert(pullRequests.count == 0)
        }
        
    }
    
    func testFetchUnexistentRepositoriesByStars() {
        
        var repositories: [Repository] = []
        
        let asyncExpectation = expectation(description: "User get request return")
        
        let completionBlock = { (repos: [Repository]) -> Void  in
            repositories = repos
            asyncExpectation.fulfill()
        }
        
        RepositoryDAO.fetchRepositoriesByStars(language: "Birlys", page: 1, completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0){ (error) in
            XCTAssert(repositories.count == 0)
        }
        
    }

    
}
