//
//  RepositoryTableViewCell.swift
//  App-iOS
//
//  Created by Judar Lima on 16/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var pullTitle: UILabel!
    @IBOutlet weak var pullBody: UILabel!
    @IBOutlet weak var photoUser: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var fullname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.photoUser.transformLayerToCircle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
