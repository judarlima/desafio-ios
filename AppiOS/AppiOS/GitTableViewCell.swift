//
//  GitTableViewCell.swift
//  App-iOS
//
//  Created by Judar Lima on 16/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit

class GitTableViewCell: UITableViewCell {

    
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var commitsQuantity: UILabel!
    @IBOutlet weak var favoriteQuantity: UILabel!
    @IBOutlet weak var photoUser: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.photoUser.transformLayerToCircle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
