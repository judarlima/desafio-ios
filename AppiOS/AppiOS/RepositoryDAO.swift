//
//  RepositoryDAO.swift
//  App-iOS
//
//  Created by Judar Lima on 17/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class RepositoryDAO: NSObject {
    
    
    //--------------------------------------------------------------------------------------------------------------------------------
    //    LIST ALL REPOSITORIES OF A LANGUAGE AND SORT BY STARS
    //--------------------------------------------------------------------------------------------------------------------------------
    
    static func fetchRepositoriesByStars(language: String, page: Int, completionHandler completion:@escaping (_ repositories : [Repository]) -> Void) {
        
        let url = "https://api.github.com/search/repositories?q=language:\(language)&sort=stars&page=\(page)"
        var repositoriesArray = [Repository]()
        
        Alamofire.request(url).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                
                if let jsonResult = response.result.value as? [String: Any] {
                    if let items = jsonResult["items"] as? [[String: Any]] {
                        for item in items{
                            if let response = Mapper<Repository>().map(JSONObject: item) {
                                repositoriesArray.append(response)
                            }
                        }
                        
                        completion(repositoriesArray)
                    }
                }
                
            case .failure(let error):
                completion(repositoriesArray)
                print(error)
            }
        }
    }
    
    
    //--------------------------------------------------------------------------------------------------------------------------------
    //    LIST ALL PULL REQUESTS IN A REPOSITORY
    //--------------------------------------------------------------------------------------------------------------------------------
    
    static func fetchRepositoryPullRequests(login: String, name: String, completionHandler completion:@escaping (_ pullRequests : [PullRequest]) -> Void) {
        
        let url = "https://api.github.com/repos/\(login)/\(name)/pulls"
        var pullRequestArray = [PullRequest]()
        
        Alamofire.request(url).validate().responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                
                if let arrayPr = response.result.value as? [[String:Any]]{
                    for pr in arrayPr {
                        if let response = Mapper<PullRequest>().map(JSONObject: pr) {
                            pullRequestArray.append(response)
                        }
                        
                    }
                    
                    completion(pullRequestArray)
                }
                
            case .failure(let error):
                completion(pullRequestArray)
                print(error)
            }
        }
    }
}




