//
//  RepositoryDetailViewController.swift
//  App-iOS
//
//  Created by Judar Lima on 16/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit
import WebImage

class RepositoryDetailViewController: UIViewController {

    let referenceRepositoryCell = "pullRequestCell"
    let referenceRepositoryCellNib = "PullRequestCell"
    var pullRequests: [PullRequest] = []
    var repository: Repository?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = self.repository?.name
        
        self.tableView.register(UINib(nibName: self.referenceRepositoryCellNib, bundle: nil), forCellReuseIdentifier: self.referenceRepositoryCell)
        
        RepositoryDAO.fetchRepositoryPullRequests(login: repository!.owner.login, name: repository!.name) { (pullRequests: [PullRequest]) in
            self.pullRequests = pullRequests
            self.tableView.reloadData()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func dismissView(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

extension RepositoryDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: referenceRepositoryCell, for: indexPath) as! PullRequestTableViewCell
        let pullRequest = pullRequests[indexPath.row]
        
        cell.fullname.text = repository!.fullName
        cell.pullBody.text = pullRequest.body
        cell.pullTitle.text = pullRequest.title
        cell.username.text = pullRequest.user.login
        cell.photoUser.sd_setImage(with: URL(string: pullRequest.user.photo), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
}

extension RepositoryDetailViewController: UITableViewDelegate {
    
}
