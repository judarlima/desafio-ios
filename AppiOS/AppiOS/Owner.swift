//
//  Owner.swift
//  App-iOS
//
//  Created by Judar Lima on 17/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit
import ObjectMapper

class Owner: NSObject, Mappable {

    var login: String!
    var photo: String!
    
    
    override init() {
        super.init()
        
    }
    
    init(login: String, photo: String) {
        self.login = login
        self.photo = photo
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        photo <- map["avatar_url"]
    }
    

}
