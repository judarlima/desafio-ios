//
//  Repository.swift
//  App-iOS
//
//  Created by Judar Lima on 16/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: NSObject, Mappable {
    
    var name: String!
    var repositoryDescription: String?
    var fullName: String!
    var forksCount: Int!
    var starsCount: Int!
    var owner: Owner = Owner()
    
    
    override init() {
        super.init()
    }
    
    init(name: String, repositoryDescription: String, fullName: String, forksCount: Int, starsCount: Int, owner: Owner) {
        self.name = name
        self.repositoryDescription = repositoryDescription
        self.owner = owner
        self.forksCount = forksCount
        self.starsCount = starsCount
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        repositoryDescription <- map["description"]
        fullName <- map["full_name"]
        forksCount <- map["forks_count"]
        starsCount <- map["stargazers_count"]
        owner <- map["owner"]
    }
    
    
}
