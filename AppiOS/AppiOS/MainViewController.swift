//
//  ViewController.swift
//  App-iOS
//
//  Created by Judar Lima on 16/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit
import WebImage

class MainViewController: UIViewController {
    
    let referenceGitCell = "gitPopCell"
    let referenceGitCellNib = "GitPopCell"
    let segueReferenceDetailRepositoryController = "DetailRepositoryControllerID"
    var repositories: [Repository] = []
    var pageCount = 1
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib(nibName: self.referenceGitCellNib, bundle: nil), forCellReuseIdentifier: self.referenceGitCell)
        
        RepositoryDAO.fetchRepositoriesByStars(language: "Java", page: pageCount){ (repositories: [Repository]) in
            self.repositories = repositories
            self.tableView.reloadData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueReferenceDetailRepositoryController {
            if let indexPath = tableView.indexPathForSelectedRow{
                print(indexPath)
                
                let destinationViewController = segue.destination as! RepositoryDetailViewController
                destinationViewController.repository = self.repositories[indexPath.row]
            }
        }
    }
    
    
    
}

extension MainViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.repositories.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: referenceGitCell, for: indexPath) as! GitTableViewCell
        let repository = self.repositories[indexPath.row]
        
        cell.name.text = repository.fullName!
        cell.repositoryName.text = repository.name!
        cell.repositoryDescription.text = repository.repositoryDescription
        cell.commitsQuantity.text = String(describing: repository.forksCount!)
        cell.favoriteQuantity.text = String(describing: repository.starsCount!)
        cell.username.text = repository.owner.login!
        cell.photoUser.sd_setImage(with: URL(string: repository.owner.photo!), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueReferenceDetailRepositoryController, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension MainViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let height = scrollView.frame.size.height
        let yOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - yOffset
        
        if(distanceFromBottom <= height) {
            
            if (self.repositories.count < 1000) {
                
                self.pageCount += 1
                
                RepositoryDAO.fetchRepositoriesByStars(language: "Java", page: pageCount){ (repositories: [Repository]) in
                    
                    for r in repositories {
                        self.repositories.append(r)
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
}
