//
//  UIImageExtensions.swift
//  App-iOS
//
//  Created by Judar Lima on 17/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    
    
    func transformLayerToCircle() {
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
//        self.layer.borderWidth = 3.0
//        self.layer.borderColor = UIColor.black.cgColor
        
    }
}
