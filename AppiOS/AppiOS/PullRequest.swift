//
//  PullRequest.swift
//  App-iOS
//
//  Created by Judar Lima on 18/11/16.
//  Copyright © 2016 Judar Lima. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: NSObject, Mappable {
    
    var title: String!
    var body: String!
    var createdAt: String!
    var user: Owner!
    
    override init() {
        super.init()
        
    }
    
    init(title: String, body: String, createdAt: String, user: Owner) {
        self.title = title
        self.body = body
        self.createdAt = createdAt
        self.user = user
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        createdAt <- map["created_at"]
        user <- map["user"]
    }

}
